"""Simple class for generation of fake names"""

import random


class FunnyNames:
    """Class provides interface to generate fake names from two lists"""

    def __init__(self):
        self.first_names = self.fill_first_names()
        self.last_names = self.fill_last_names()

    @staticmethod
    def fill_first_names():
        """
        :rtype: list
        :return: list of available first names
        """
        return ['Allan', 'Jonson', 'Will']

    @staticmethod
    def fill_last_names():
        """
        :rtype: list
        :return: list of available last names
        """
        return ['Sheppard', 'Ankwalden', 'Erricson']

    def get_first_name(self):
        """
        :rtype: str
        :return: one of available first names selected randomly
        """
        return random.choice(self.first_names)

    def get_last_name(self):
        """
        :rtype: str
        :return: one of available last names selected randomly
        """
        return random.choice(self.last_names)
