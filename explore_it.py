"""Program generates fake names"""

import os

from SourceCode import FunnyNames, BaseColors

os.system("")

if __name__ == '__main__':
    name = FunnyNames()

    for _ in range(5):
        print(f'{BaseColors.OKGREEN}{name.get_last_name()}, {name.get_first_name()}{BaseColors.ENDC}')
