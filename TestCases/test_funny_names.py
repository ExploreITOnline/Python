"""Tests for FunnyNames class"""

from mock import patch

from SourceCode import FunnyNames


class TestFunnyNames:
    """ Base test class"""

    @patch('SourceCode.FunnyNames.fill_first_names', return_value=['Will'])
    def test_get_first_name(self, expected):
        names = FunnyNames()
        actual = names.get_first_name()
        assert actual == expected.return_value[0], "Method 'get_first_name()' returns unexpected value"

    @patch('SourceCode.FunnyNames.fill_last_names', return_value=['Smith', 'Werber'])
    def test_get_last_name(self, expected):
        names = FunnyNames()
        actual = names.get_last_name()
        assert actual in expected.return_value, "Method 'get_last_name()' returns unexpected value"
